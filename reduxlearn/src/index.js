import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {createStore} from 'redux';

//ACTION --> Describes what you wanna do --> Function that returns object
const increment = () => {
  console.log("increment")
  return {
    type: 'INCREMENT' //name
  }
}
const decrement = () => {
  console.log("decrement")
  return {
    type: 'DECREMENT' //name
  }
}

//REDUCER --> Describes how your ACTIONs transform the state into the next state
const counter = (state = 0 , action) =>{
  console.log("counter")
  switch(action.type){
    case "INCREMENT":
      return state + 1;
    case "DECREMENT":
      return state - 1;  
  }
}

//STORE --> Globalized STATE
let store = createStore(counter)

// store.subscribe(() => console.log("state: "+store.getState()));

//DISPATCH --> Execute that action
store.dispatch(increment())

//Display in console
console.log("state: "+store.getState())

store.dispatch(decrement())
console.log("state: "+store.getState())

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
