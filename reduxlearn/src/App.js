
function App() {
  return (
    <div className="App">
      This is how redux works. <br/>
      Step 1 : store.dispatch(increment()) <br/>
      Step 2 : const increment = () action is called and it return the type <br/>
      Step 3 : const counter = (state = 0 , action) checks the type of action returned and execute the relevant method
    </div>
  );
}

export default App;
